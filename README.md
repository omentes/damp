# D.A.M.P aka Docker Apache MariaDB Php

Small web server for Wordpress site developing.
Docker composer for Wordpress with MariaDB, include PhpMyAdmin and restore/backup SQL scripts on bash

## DESCRIPTION

This container will help you deploy a website development on Wordpress to any Unix system. All environment variables are specified in the files. There are also scripts for saving and restoring the database.

## INSTALL

```
git clone https://gitlab.com/omentes/damp.git
cd damp
docker-compose up

```

## ACCESS

Website:
```
http://127.0.0.1/
```

PhpMyAdmin:
```
http://127.0.0.1:8080/
```

ENV SQL:
```
db:3306
wordpress
wordpress
wordpress
```

Backup and resore SQL scipts:
```
sh backup.sh
sh restore.sh
```

## Built With

* Docker compose
* Apache
* MariaDB
* Wordpress

## Authors

*  **Artem Pakhomov** - [apakhomo](https://gitlab.com/omentes/)

## License

This project is licensed under the MIT License
